import cors from 'cors'
import { createServer } from '@gitts/core'

const server = createServer()
// CORS configuration
server.use(cors())
server.options('*', cors())

// Start server
server.start()