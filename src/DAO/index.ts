import { DbContext } from "@gitts/core";
import { User } from "../entity/user.entity";

export class DAO {
    static get users() {
        return DbContext.getConnection().getRepository(User)
    }
}