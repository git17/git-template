import { Injectable } from "@gitts/core";
import { DAO } from "../DAO";
import { User } from "../entity/user.entity";

@Injectable()
export class UserService {
    
    async getAllUsers() {
        return await DAO.users.find()
    }

    async createUser(data: Partial<User>) {
        const newUser = new User()

        newUser.firstName = data.firstName
        newUser.lastName = data.lastName

        await DAO.users.save(newUser)

        return newUser
    }

    async updateUser(id: number, data: Partial<User>) {

        const user = await DAO.users.findOne(id)
        if (!user) return null

        user.firstName = data.firstName || user.firstName
        user.lastName = data.lastName || user.lastName

        await DAO.users.update(user.id, user)

        return user
    }

    async deleteUser(id: number) {
        await DAO.users.delete(id)
    }
}