import { Controller, Get, Post, Patch, Delete, Inject } from "@gitts/core"
import { UserService } from "../services/user.service"

@Controller('/users')
export class UserController {
    @Inject(UserService)
    userService: UserService

    @Get({
        path: ''
    })
    async getUsers(req, res) {
        const users = await this.userService.getAllUsers()
        res.json(users)
    }

    @Post({
        path: ''
    })
    async createUser(req, res) {
        const { body } = req

        const newUser = await this.userService.createUser(body)

        res.json(newUser)
    }

    @Patch({
        path: '/:id'
    })
    async updateUser(req, res) {
        let { body } = req,
            { id } = req.params

        const user = await this.userService.updateUser(parseInt(id), body)

        if (!user) return res.status(404).json({
            message: 'User not found'
        })

        res.json(user)
    }

    @Delete({
        path: '/:id'
    })
    async deleteUser(req, res) {
        let { id } = req.params
        await this.userService.deleteUser(parseInt(id))
        res.sendStatus(204)
    }
}