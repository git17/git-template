import { Entity, Column, PrimaryGeneratedColumn } from "@gitts/core";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    firstName: string

    @Column()
    lastName: string
}
