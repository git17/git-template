const fs = require('fs')
const path = require('path')
const glob = require('glob')
const AdmZip = require('adm-zip')

// absolute paths
const cwd = process.cwd()
const templatesFolder = path.resolve(cwd, './templates')
const defaultTemplateFolder = path.resolve(templatesFolder, './default')
const srcPath = path.resolve(cwd, './src')
const srcFilesPattern = path.resolve(cwd, './src/**/*')
const buildFoldersPattern = path.resolve(templatesFolder, '*')

function isFile(path) {
    const pathComponents = path.split('/')
    const lastPath = pathComponents[pathComponents.length - 1]
    if (!lastPath.match(/\./g)) return false
    return true
}

function createFileFoldersIfNotExist(filePath, startPath = '') {
    const pathComponents = filePath.split('/')

    pathComponents.forEach((dir, index) => {
        if (index === pathComponents.length - 1) return
        let absolutePath = path.resolve(startPath, dir)
        if (!fs.existsSync(absolutePath)) {
            fs.mkdirSync(absolutePath)
        }
        startPath = absolutePath
        console.log('Created folder: ', absolutePath)
    })
}

function getSourceFiles() {
    return new Promise((rs, rj) => {
        glob(srcFilesPattern, { dot: true }, (err, files) => {
            if (err) return rj(err)
            files = files.filter(file => isFile(file))
            rs(files)
        })
    })
}

function handleError(error, exit = true) {
    console.error(error)
    if (exit) process.exit()
}

function copyFile(from, to) {
    fs.copyFileSync(from, to)
}

function createOuterStuff(desPath) {
    const files = [
        './.env.pattern', './.gitignore.pattern',
        './package.pattern.json',
        './README.md',
        './tsconfig.json']
    files.forEach(file => {
        let absolutePath = path.resolve(cwd, file)
        let absoluteDesPath = path.resolve(cwd, desPath, file)
        try {
            copyFile(absolutePath, absoluteDesPath)
        } catch (err) {
            console.error('Failed to copy file: ' + file, err)
        }
    })
}

async function createDefaultTemplate() {
    try {
        var srcFiles = await getSourceFiles()
    } catch (error) {
        handleError(error)
    }
    srcFiles = srcFiles.filter(filePath => !filePath.match(/\.log/g))
    createFileFoldersIfNotExist('templates/default/')
    srcFiles.forEach(filePath => {
        let relativeFilePath = path.relative(cwd, filePath).replace(/\\/g, '/')
        createFileFoldersIfNotExist(relativeFilePath, defaultTemplateFolder)
        let absoluteFilePath = path.resolve(defaultTemplateFolder, relativeFilePath)
        try {
            copyFile(filePath, absoluteFilePath)
        } catch (err) {
            console.error('Failed to copy file: ' + relativeFilePath, err)
        }
    })
    createOuterStuff('./templates/default')
}

function getBuildFolders() {
    return new Promise((rs, rj) => {
        glob(buildFoldersPattern, { dot: true }, (err, files) => {
            if (err) return rj(err)
            rs(files)
        })
    })
}

function getTemplateFiles(templatePath) {
    let pattern = path.resolve(templatePath, '**/*')

    return new Promise((rs, rj) => {
        glob(pattern, { dot: true }, (err, files) => {
            if (err) return rj(err)
            rs(files)
        })
    })
}

function getContainerFolder(path) {
    const components = Array.from(path.split('/'))
    components.pop()
    return components.join('/')
}

async function compress() {
    const outDir = path.resolve(cwd, './builds')
    let buildFolderPaths = await getBuildFolders()
    buildFolderPaths.forEach(async (folderPath) => {
        let files = await getTemplateFiles(folderPath)
        // let folders = files.filter(filePath => !isFile(filePath))
        files = files.filter(filePath => isFile(filePath))
        let zip = new AdmZip()
        files.forEach(filePath => {
            let zipFilePath = path.relative(folderPath, filePath).replace(/\\/g, '/')
            let zipDes = getContainerFolder(zipFilePath)
            zip.addLocalFile(filePath, zipDes)
        })

        let pathComponents = folderPath.split('/')
        let buildFolderName = pathComponents[pathComponents.length - 1]
        let outFile = path.resolve(outDir, `${buildFolderName}.zip`)

        zip.writeZip(outFile)
    })
}

function build() {
    return new Promise(async (rs, rj) => {
        try {
            console.clear()
            await createDefaultTemplate()
            await compress()
            rs()
        } catch (error) {
            rj(error)
        }
    })
}

module.exports = build().then(() => {
    console.log('Build done!')
})