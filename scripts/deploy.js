const arg = require('arg')
const inquirer = require('inquirer')
const path = require('path')
const glob = require('glob')
const S3 = require('aws-sdk/clients/s3')
const fs = require('fs')
const dotenv = require('dotenv')
dotenv.config()

const rawArgs = process.argv
const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID
const AWS_ACCESS_KEY_SECRET = process.env.AWS_ACCESS_KEY_SECRET
const S3_BUCKET = process.env.S3_BUCKET
const cwd = process.cwd()
const buildsDir = path.resolve(cwd, './builds')

const s3Client = new S3({
    apiVersion: '2006-03-01',
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_ACCESS_KEY_SECRET,
    region: 'ap-southeast-1'
})

function parseArgs() {
    const args = arg({}, {
        argv: rawArgs.slice(2),
    })

    return {
        version: args._[0]
    }
}

async function ask(options) {
    if (options.version) return options;

    const questions = []

    questions.push({
        name: 'version',
        message: 'Version?'
    })

    const answers = await inquirer.prompt(questions)

    return {
        ...options,
        version: options.version || answers.version
    }
}

function getBuilds() {
    return new Promise((rs, rj) => {
        glob(`${buildsDir}\\*`, (err, files) => {
            if (err) return rj(err)
            rs(files)
        })
    })
}

function handleError(err, exit = true) {
    console.error(err)
    if (exit) process.exit(1)
}

function getFileName(path) {
    return path.split('/').pop()
}

function getUploader(version) {
    return async (filePath) => {
        const fileName = getFileName(filePath)
        const key = `${version}/${fileName}`

        // console.log(params)
        fs.readFile(filePath, (err, data) => {
            if (err) return console.error('Error on uploading ' + fileName, err)
            const params = {
                ACL: 'public-read',
                Body: data,
                Bucket: S3_BUCKET,
                Key: key
            }
            s3Client.upload(params, (err, data) => {
                if (err) {
                    console.error('Error on uploading ' + fileName, err)
                    return
                }
                console.log('Successfully uploaded ' + fileName)
            })
        })
    }
}

(async function () {
    let options = parseArgs()
    options = await ask(options)
    if (!options.version) {
        throw new Error('You did not specify the version!')
    }

    try {
        console.log('Version:', options.version)
        console.log('Deploying...')
        const upload = getUploader(options.version)
        const files = await getBuilds()
        files.forEach(upload)
    } catch (error) {
        handleError(error)
    }
})()